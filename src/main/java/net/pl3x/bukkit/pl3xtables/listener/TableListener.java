package net.pl3x.bukkit.pl3xtables.listener;

import net.pl3x.bukkit.pl3xtables.Blueprint;
import net.pl3x.bukkit.pl3xtables.Chat;
import net.pl3x.bukkit.pl3xtables.api.event.TablePlaceEvent;
import net.pl3x.bukkit.pl3xtables.configuration.Lang;
import net.pl3x.bukkit.pl3xtables.configuration.TableConfig;
import net.pl3x.bukkit.pl3xtables.table.Table;
import net.pl3x.bukkit.pl3xtables.table.TableManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class TableListener implements Listener {
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPistonExtend(BlockPistonExtendEvent event) {
        TableManager tableManager = TableManager.getManager();
        for (Block block : event.getBlocks()) {
            if (tableManager.getTable(block.getLocation()) != null) {
                event.setCancelled(true);
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPistonRetract(BlockPistonRetractEvent event) {
        TableManager tableManager = TableManager.getManager();
        for (Block block : event.getBlocks()) {
            if (tableManager.getTable(block.getLocation()) != null) {
                event.setCancelled(true);
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        TableManager tableManager = TableManager.getManager();
        Table table = tableManager.getTable(event.getBlock().getLocation());
        if (table != null) {
            tableManager.removeTable(table);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        TableManager tableManager = TableManager.getManager();
        Table table = tableManager.getTable(event.getBlock().getLocation().add(0, -1, 0));
        if (table != null) {
            new Chat(Lang.CANNOT_PLACE_BLOCKS_ON_TABLES).send(event.getPlayer());
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onFallingBlockOnTable(EntityChangeBlockEvent event) {
        if (!(event.getEntity() instanceof FallingBlock)) {
            return; // not a falling block
        }
        TableManager tableManager = TableManager.getManager();
        Table table = tableManager.getTable(event.getBlock().getLocation().add(0, -1, 0));
        if (table == null) {
            return;
        }

        Location loc = event.getBlock().getLocation();
        //noinspection deprecation
        loc.getWorld().dropItemNaturally(loc, new ItemStack(event.getTo(), 1, event.getData(), event.getData()));
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerPlaceOrBreakTable(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (player.isSneaking()) {
            return; // ignore shift clicks
        }

        // left click breaks table
        if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
            TableManager tableManager = TableManager.getManager();
            Table table = tableManager.getTable(event.getClickedBlock().getLocation());
            if (table != null) {
                Location loc = table.getLocation();
                loc.getWorld().dropItemNaturally(loc, Blueprint.TABLE.getItem());
                tableManager.removeTable(table);
                event.setCancelled(true);
            }
            return;
        }

        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return; // not right clicking
        }

        ItemStack hand = event.getItem();
        if (!Blueprint.TABLE.isItem(hand)) {
            return; // not a table blueprint
        }

        Block block = event.getClickedBlock();
        if (block == null || block.getType() == Material.AIR) {
            return; // clicked nothing/air
        }

        Block targetBlock = block.getRelative(event.getBlockFace());
        TableManager tableManager = TableManager.getManager();
        if (tableManager.getTable(targetBlock.getRelative(BlockFace.DOWN).getLocation()) != null) {
            new Chat(Lang.CANNOT_STACK_TABLES).send(player);
            event.setCancelled(true);
            return;
        }

        if (targetBlock.getType() != Material.AIR) {
            return;
        }

        Location targetLoc = targetBlock.getLocation();
        if (tableManager.getTable(targetLoc) != null) {
            new Chat(Lang.TABLE_ALREADY_THERE).send(player);
            event.setCancelled(true);
            return;
        }

        Table table = new Table(targetLoc);

        TablePlaceEvent tableEvent = new TablePlaceEvent(player, table);
        Bukkit.getPluginManager().callEvent(tableEvent);
        if (tableEvent.isCancelled()) {
            event.setCancelled(true);
            return; // a plugin stopped the table placement
        }
        table.init();

        TableConfig.getConfig(targetLoc).setTable(table);
        tableManager.addTable(table);

        if (hand.getAmount() > 1) {
            hand.setAmount(hand.getAmount() - 1);
        } else {
            hand = null;
        }

        if (event.getHand() == EquipmentSlot.HAND) {
            player.getInventory().setItemInMainHand(hand);
        } else if (event.getHand() == EquipmentSlot.OFF_HAND) {
            player.getInventory().setItemInOffHand(hand);
        }

        //noinspection deprecation
        player.updateInventory();
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerInteractTable(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (player.isSneaking()) {
            return; // ignore shift clicks
        }

        if (event.getHand() == EquipmentSlot.OFF_HAND && event.getItem() == null) {
            return; // ignoring 2nd empty hand packet
        }

        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return; // not right clicking
        }

        Block block = event.getClickedBlock();
        if (block == null || block.getType() != Material.BARRIER) {
            return; // not a barrier block
        }

        Table table = TableManager.getManager().getTable(block.getLocation());
        if (table == null) {
            return; // not a table
        }

        // remove item from table
        if (event.getItem() == null || event.getItem().getType() == Material.AIR) {
            ItemStack item = table.removeItem(player);
            if (item != null && item.getType() != Material.AIR) {
                player.getInventory().setItem(player.getInventory().getHeldItemSlot(), item);
                //noinspection deprecation
                player.updateInventory();
                event.setCancelled(true);
            }
            return; // finished
        }

        // add item to table
        ItemStack item = event.getItem();
        if (table.addItem(player, item)) {
            // remove item from player
            if (item.getAmount() > 1) {
                // player has more than 1 item
                // only take 1 away instead of removing completely
                item.setAmount(item.getAmount() - 1);
            } else {
                // only has 1 item, remove completely
                item = null;
            }
            // remove from correct hand used
            if (event.getHand() == EquipmentSlot.OFF_HAND) {
                player.getInventory().setItemInOffHand(item);
            } else {
                player.getInventory().setItemInMainHand(item);
            }
            //noinspection deprecation
            player.updateInventory();
            event.setCancelled(true);
        }
    }
}
