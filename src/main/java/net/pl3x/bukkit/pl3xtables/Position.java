package net.pl3x.bukkit.pl3xtables;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

@SuppressWarnings("WeakerAccess")
public class Position {
    private final Vector offset;
    private final double yaw;
    private final EulerAngle rotation;
    private final EquipmentSlot slot;
    private final boolean small;

    public Position(Vector offset, double yaw, EulerAngle rotation) {
        this(offset, yaw, rotation, false, 0);
    }

    public Position(Vector offset, double yaw, EulerAngle rotation, boolean small, int slot) {
        this.offset = offset;
        this.yaw = yaw;
        this.rotation = rotation;
        this.slot = EquipmentSlot.values()[slot];
        this.small = small;
    }

    public Vector getOffset() {
        return offset;
    }

    public double getYaw() {
        return yaw;
    }

    public EulerAngle getRotation() {
        return rotation;
    }

    public boolean isSmall() {
        return small;
    }

    public EquipmentSlot getSlot() {
        return slot;
    }

    public ArmorStand spawn(Location location) {
        Location loc = location.clone().add(getOffset());
        loc.setYaw((float) getYaw());
        ArmorStand stand = (ArmorStand) loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
        stand.setGravity(false);
        stand.setAI(false);
        stand.setVisible(false);
        stand.setMarker(true); // THIS BREAKS LIGHTING!!!
        stand.setBasePlate(false);
        stand.setCanPickupItems(false);
        stand.setCollidable(false);
        stand.setInvulnerable(true);
        stand.setRightArmPose(getRotation());
        return stand;
    }
}
