package net.pl3x.bukkit.pl3xtables;

import net.pl3x.bukkit.pl3xtables.configuration.PositionsConfig;
import net.pl3x.bukkit.pl3xtables.table.Table;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

@SuppressWarnings("WeakerAccess")
public class ContentItem {
    private final Table table;
    private final ArmorStand armorStand;
    private final int corner;
    private Position position;

    public ContentItem(Table table, int corner) {
        this.table = table;
        position = new Position(new Vector(0, -0.4, 0), 0, new EulerAngle(0, 0, 0));
        this.armorStand = position.spawn(table.getLocation());
        this.corner = corner;
        setPosition(position);
    }

    public Table getTable() {
        return table;
    }

    public ArmorStand getArmorStand() {
        return armorStand;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
        Location location = getTable().getLocation();
        getArmorStand().setAI(true);
        getArmorStand().teleport(new Location(
                location.getWorld(),
                location.getX() + position.getOffset().getX(),
                location.getY() + position.getOffset().getY(),
                location.getZ() + position.getOffset().getZ()));
        getArmorStand().setAI(false);
        getArmorStand().setRightArmPose(position.getRotation());
        getArmorStand().setSmall(position.isSmall());
    }

    public ItemStack getItem() {
        switch (getPosition().getSlot()) {
            case FEET:
                return getArmorStand().getBoots();
            case LEGS:
                return getArmorStand().getLeggings();
            case CHEST:
                return getArmorStand().getChestplate();
            case HEAD:
                return getArmorStand().getHelmet();
            default:
                return getArmorStand().getItemInHand();
        }
    }

    public boolean setItem(ItemStack item) {
        if (item != null) {
            if (item.getType() == Material.AIR) {
                Logger.debug("Cannot set air");
                return false;
            }
            Position position = PositionsConfig.getConfig().getPosition(item.getType(), corner);
            if (position == null) {
                Logger.debug("No entry in positions.yml for " + item.getType().name());
                return false;
            }
            setPosition(position);
        }
        switch (getPosition().getSlot()) {
            case FEET:
                getArmorStand().setBoots(item);
                break;
            case LEGS:
                getArmorStand().setLeggings(item);
                break;
            case CHEST:
                getArmorStand().setChestplate(item);
                break;
            case HEAD:
                getArmorStand().setHelmet(item);
                break;
            default:
                getArmorStand().setItemInHand(item);
        }
        return true;
    }

    public boolean removeItem(ItemStack item) {
        if (item == null || item.getType() == Material.AIR) {
            Logger.debug("Cannot remove null or air");
            return false;
        }
        setItem(null);
        return true;
    }

    public void dropItem() {
        ItemStack item = getItem();
        if (item != null && item.getType() != Material.AIR) {
            getArmorStand().getLocation().getWorld().dropItemNaturally(getArmorStand().getLocation(), item);
            setItem(null);
        }
    }

    public void despawn(boolean dropItem) {
        if (dropItem) {
            dropItem();
        }
        getArmorStand().remove();
        position = null;
    }
}
