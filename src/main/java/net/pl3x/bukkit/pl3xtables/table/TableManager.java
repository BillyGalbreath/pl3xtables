package net.pl3x.bukkit.pl3xtables.table;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import net.pl3x.bukkit.pl3xtables.Logger;
import net.pl3x.bukkit.pl3xtables.configuration.TableConfig;
import org.bukkit.Location;

public class TableManager {
    private static TableManager manager;

    public static TableManager getManager() {
        if (manager == null) {
            manager = new TableManager();
        }
        return manager;
    }

    private final Set<Table> tables = new HashSet<>();

    public void addTable(Table table) {
        tables.add(table);
    }

    public Table getTable(Location location) {
        for (Table table : tables) {
            if (table.getLocation().equals(location)) {
                return table;
            }
        }
        return null;
    }

    public void removeTable(Table table) {
        Logger.debug("Deleting table at " + table.getLocation());
        TableConfig config = TableConfig.getConfig(table);
        if (config.delete()) {
            table.destroy(true);
            tables.remove(table);
        }
    }

    public void unloadAll() {
        Iterator<Table> iter = tables.iterator();
        while (iter.hasNext()) {
            Table table = iter.next();
            Logger.debug("Unloading table at " + table.getLocation());
            table.destroy(false);
            iter.remove();
        }
    }

    public void loadAll() {
        TableConfig.loadAll();
    }
}
