package net.pl3x.bukkit.pl3xtables.table;

import java.util.ArrayList;
import java.util.List;
import net.pl3x.bukkit.pl3xtables.ContentItem;
import net.pl3x.bukkit.pl3xtables.Logger;
import net.pl3x.bukkit.pl3xtables.Position;
import net.pl3x.bukkit.pl3xtables.api.event.AddItemEvent;
import net.pl3x.bukkit.pl3xtables.api.event.RemoveItemEvent;
import net.pl3x.bukkit.pl3xtables.configuration.Config;
import net.pl3x.bukkit.pl3xtables.configuration.TableConfig;
import net.pl3x.bukkit.pl3xtables.task.FixLight;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

@SuppressWarnings("WeakerAccess")
public class Table {
    private final Location location;
    private final ArmorStand[] tableStands = new ArmorStand[8];
    private final ContentItem[] contentItems = new ContentItem[4];

    public Table(Location location) {
        this.location = location;
    }

    public void init() {
        location.getBlock().setType(Material.BARRIER);

        // setup table
        tableStands[0] = new Position(new Vector(0.0, -0.42, 0.7), 45, new EulerAngle(1.41, 0, 0)).spawn(getLocation());
        tableStands[0].setItemInHand(new ItemStack(Material.STICK));
        tableStands[1] = new Position(new Vector(0.3, -0.42, 0.0), 135, new EulerAngle(1.41, 0, 0)).spawn(getLocation());
        tableStands[1].setItemInHand(new ItemStack(Material.STICK));
        tableStands[2] = new Position(new Vector(0.999, -0.42, 0.3), 225, new EulerAngle(1.41, 0, 0)).spawn(getLocation());
        tableStands[2].setItemInHand(new ItemStack(Material.STICK));
        tableStands[3] = new Position(new Vector(0.7, -0.42, 0.999), 315, new EulerAngle(1.41, 0, 0)).spawn(getLocation());
        tableStands[3].setItemInHand(new ItemStack(Material.STICK));
        tableStands[4] = new Position(new Vector(0.772, -0.41, 0.772), 0, new EulerAngle(0, 0, 0)).spawn(getLocation());
        tableStands[4].setHelmet(new ItemStack(Material.WOOD_PLATE));
        tableStands[5] = new Position(new Vector(0.228, -0.41, 0.772), 0, new EulerAngle(0, 0, 0)).spawn(getLocation());
        tableStands[5].setHelmet(new ItemStack(Material.WOOD_PLATE));
        tableStands[6] = new Position(new Vector(0.228, -0.41, 0.228), 0, new EulerAngle(0, 0, 0)).spawn(getLocation());
        tableStands[6].setHelmet(new ItemStack(Material.WOOD_PLATE));
        tableStands[7] = new Position(new Vector(0.772, -0.41, 0.228), 0, new EulerAngle(0, 0, 0)).spawn(getLocation());
        tableStands[7].setHelmet(new ItemStack(Material.WOOD_PLATE));

        // setup table contents
        contentItems[0] = new ContentItem(this, 0);
        contentItems[1] = new ContentItem(this, 1);
        contentItems[2] = new ContentItem(this, 2);
        contentItems[3] = new ContentItem(this, 3);

        // add table to light fix task
        if (Config.SETTINGS__LIGHT_FIX__ENABLED.getBoolean()) {
            FixLight.getTask().addLocation(getLocation());
        }
    }

    public Location getLocation() {
        return location.clone();
    }

    public void destroy(boolean dropContents) {
        // remove table from light fix task
        if (Config.SETTINGS__LIGHT_FIX__ENABLED.getBoolean()) {
            FixLight.getTask().removeLocation(getLocation());
        }

        // remove barrier block
        location.getBlock().setType(Material.AIR);

        // remove content armorstands from world
        for (int i = 0; i < contentItems.length; i++) {
            if (contentItems[i] != null) {
                contentItems[i].despawn(dropContents);
                contentItems[i] = null;
            }
        }

        // remove table armorstands from world
        for (int i = 0; i < tableStands.length; i++) {
            if (tableStands[i] != null) {
                tableStands[i].remove(); // kill armorstand
                tableStands[i] = null;
            }
        }
    }

    public List<ItemStack> getItems() {
        List<ItemStack> items = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            ItemStack item = contentItems[i].getItem();
            if (item == null || item.getType() == Material.AIR) {
                items.add(null);
            } else {
                items.add(item);
            }
        }
        return items;
    }

    private int getItemCount() {
        int count = 0;
        for (int i = 0; i < 4; i++) {
            ItemStack item = contentItems[i].getItem();
            if (item != null && item.getType() != Material.AIR) {
                count++;
            }
        }
        return count;
    }

    public boolean addItem(Player player, ItemStack item) {
        int count = getItemCount();
        if (count >= 4) {
            Logger.debug("Table full");
            return false; // table already full
        }
        if (item != null) {
            item = item.clone(); // lets not modify the original stack
            item.setAmount(1);
        }

        if (player != null) {
            AddItemEvent event = new AddItemEvent(player, this, item);
            Bukkit.getPluginManager().callEvent(event);
            if (event.isCancelled()) {
                Logger.debug("Another plugin cancelled item addition");
                return false;
            }
            item = event.getItem();
        }

        if (!contentItems[count].setItem(item)) {
            Logger.debug("Unable to set");
            return false; // unable to set item
        }
        TableConfig.getConfig(getLocation()).setContents(this);
        return true;
    }

    public ItemStack removeItem(Player player) {
        int count = getItemCount();
        if (count == 0) {
            return null;
        }
        count -= 1;
        ItemStack item = contentItems[count].getItem();
        if (item == null || item.getType() == Material.AIR) {
            return null;
        }

        if (player != null) {
            RemoveItemEvent event = new RemoveItemEvent(player, this, item);
            Bukkit.getPluginManager().callEvent(event);
            if (event.isCancelled()) {
                Logger.debug("Another plugin cancelled item removal");
                return null;
            }
        }

        if (contentItems[count].removeItem(item)) {
            TableConfig.getConfig(getLocation()).setContents(this);
            return item;
        }

        return null;
    }
}
