package net.pl3x.bukkit.pl3xtables.task;

import com.google.common.collect.Lists;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.minecraft.server.v1_10_R1.BlockPosition;
import net.minecraft.server.v1_10_R1.EnumSkyBlock;
import net.minecraft.server.v1_10_R1.PacketPlayOutMapChunk;
import net.minecraft.server.v1_10_R1.WorldServer;
import net.pl3x.bukkit.pl3xtables.Pl3xTables;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_10_R1.CraftChunk;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.plugin.IllegalPluginAccessException;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.NumberConversions;

public class FixLight extends BukkitRunnable {
    private static FixLight fixLightTask;

    public static FixLight getTask() {
        if (fixLightTask == null) {
            fixLightTask = new FixLight();
        }
        return fixLightTask;
    }

    private boolean cancelled = false;
    private final Set<Location> tableLocations = new HashSet<>();
    private Field fE;

    private FixLight() {
        try {
            fE = PacketPlayOutMapChunk.class.getDeclaredField("e");
            fE.setAccessible(true);
        } catch (NoSuchFieldException ignore) {
        }
    }

    public void addLocation(Location tableLocation) {
        tableLocation = tableLocation.clone();
        tableLocation.setX(tableLocation.getBlockX());
        tableLocation.setY(tableLocation.getBlockY());
        tableLocation.setZ(tableLocation.getBlockZ());
        tableLocation.setYaw(0);
        tableLocation.setPitch(0);
        tableLocations.add(tableLocation);
    }

    @SuppressWarnings("deprecation")
    public void removeLocation(Location tableLocation) {
        // cleanup light fix
        Block block = tableLocation.getBlock().getRelative(BlockFace.DOWN);
        int type = block.getTypeId();
        byte data = block.getData();
        block.setType(Material.GLOWSTONE);
        try {
            Bukkit.getScheduler().runTaskLater(Pl3xTables.getPlugin(),
                    () -> block.setTypeIdAndData(type, data, true), 2);
        } catch (IllegalPluginAccessException ignore) {
            block.setTypeIdAndData(type, data, true);
        }

        tableLocations.remove(tableLocation);
    }

    private Set<Location> getLocations(Location location) {
        Set<Location> locs = new HashSet<>();
        locs.add(location.clone().add(0, -1, 0)); // below
        locs.add(location.clone().add(1, -1, 0)); // east
        locs.add(location.clone().add(1, -1, 1)); // southeast
        locs.add(location.clone().add(0, -1, 1)); // south
        locs.add(location.clone().add(-1, -1, 1)); // southwest
        locs.add(location.clone().add(-1, -1, 0)); // west
        locs.add(location.clone().add(-1, -1, -1)); // northwest
        locs.add(location.clone().add(0, -1, -1)); // north
        locs.add(location.clone().add(1, -1, -1)); // northeast
        return locs;
    }

    @Override
    public void run() {
        if (cancelled) {
            return;
        }
        doIt(true);
    }

    @Override
    public void cancel() {
        super.cancel();
        doIt(false);
        cancelled = true;
        tableLocations.clear();
    }

    private double getDistance(Chunk chunk1, Chunk chunk2) {
        return NumberConversions.square(chunk1.getX() - chunk2.getX())
                + NumberConversions.square(chunk1.getZ() - chunk2.getZ());
    }

    private void doIt(boolean applyLight) {
        if (tableLocations.isEmpty()) {
            return;
        }

        Map<Chunk, PacketPlayOutMapChunk> packets = new HashMap<>();
        Map<Chunk, Map<BlockPosition, Byte>> chunkLights = new HashMap<>();

        // build data vars
        for (Location tableLoc : tableLocations) {
            Chunk chunk = tableLoc.getChunk();
            if (!chunk.isLoaded()) {
                continue; // not loaded
            }
            Map<BlockPosition, Byte> blockLights = chunkLights.get(chunk);
            if (blockLights == null) {
                blockLights = new HashMap<>();
            }
            for (Location lightLoc : getLocations(tableLoc)) {
                BlockPosition position = new BlockPosition(lightLoc.getX(), lightLoc.getY(), lightLoc.getZ());
                blockLights.put(position, lightLoc.getBlock().getLightLevel());
            }
            chunkLights.put(chunk, blockLights);
        }

        if (chunkLights.isEmpty()) {
            return;
        }

        // apply new light levels
        if (applyLight) {
            for (Map.Entry<Chunk, Map<BlockPosition, Byte>> entry : chunkLights.entrySet()) {
                WorldServer nmsWorld = ((CraftWorld) entry.getKey().getWorld()).getHandle();
                for (BlockPosition position : entry.getValue().keySet()) {
                    nmsWorld.a(EnumSkyBlock.BLOCK, position, 8);
                }
            }
        }

        // build update chunk packet and send packet to all nearby players
        for (Chunk chunk : chunkLights.keySet()) {
            packets.put(chunk, getPacket(chunk));

            // add neighboring chunks
            World world = chunk.getWorld();
            Chunk c = world.getChunkAt(chunk.getX() + 1, chunk.getZ());
            if (!packets.containsKey(c)) {
                packets.put(c, getPacket(c));
            }
            c = world.getChunkAt(chunk.getX() + 1, chunk.getZ() + 1);
            if (!packets.containsKey(c)) {
                packets.put(c, getPacket(c));
            }
            c = world.getChunkAt(chunk.getX(), chunk.getZ() + 1);
            if (!packets.containsKey(c)) {
                packets.put(c, getPacket(c));
            }
            c = world.getChunkAt(chunk.getX() - 1, chunk.getZ() + 1);
            if (!packets.containsKey(c)) {
                packets.put(c, getPacket(c));
            }
            c = world.getChunkAt(chunk.getX() - 1, chunk.getZ());
            if (!packets.containsKey(c)) {
                packets.put(c, getPacket(c));
            }
            c = world.getChunkAt(chunk.getX() - 1, chunk.getZ() - 1);
            if (!packets.containsKey(c)) {
                packets.put(c, getPacket(c));
            }
            c = world.getChunkAt(chunk.getX(), chunk.getZ() - 1);
            if (!packets.containsKey(c)) {
                packets.put(c, getPacket(c));
            }
            c = world.getChunkAt(chunk.getX() + 1, chunk.getZ() - 1);
            if (!packets.containsKey(c)) {
                packets.put(c, getPacket(c));
            }
        }

        for (Map.Entry<Chunk, PacketPlayOutMapChunk> entry : packets.entrySet()) {
            Bukkit.getOnlinePlayers().stream()
                    .filter(target -> getDistance(target.getLocation().getChunk(), entry.getKey()) <= 5)
                    .forEach(target -> ((CraftPlayer) target).getHandle()
                            .playerConnection.sendPacket(entry.getValue()));
        }


        // apply old light level back
        for (Map.Entry<Chunk, Map<BlockPosition, Byte>> entry : chunkLights.entrySet()) {
            WorldServer nmsWorld = ((CraftWorld) entry.getKey().getWorld()).getHandle();
            for (Map.Entry<BlockPosition, Byte> light : entry.getValue().entrySet()) {
                nmsWorld.a(EnumSkyBlock.BLOCK, light.getKey(), light.getValue());
            }
        }
    }

    private PacketPlayOutMapChunk getPacket(Chunk chunk) {
        PacketPlayOutMapChunk packet = new PacketPlayOutMapChunk(((CraftChunk) chunk).getHandle(), 65534);
        try {
            if (fE != null) {
                // remove NBT data from packet (fixes Pl3xSigns)
                fE.set(packet, Lists.newArrayList());
            }
        } catch (IllegalAccessException ignore) {
        }
        return packet;
    }
}
