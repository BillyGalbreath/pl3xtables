package net.pl3x.bukkit.pl3xtables.configuration;

import net.pl3x.bukkit.pl3xtables.Pl3xTables;

public enum Config {
    SETTINGS__COLOR_LOGS(true),
    SETTINGS__DEBUG_MODE(false),
    SETTINGS__LANGUAGE_FILE("lang-en.yml"),
    SETTINGS__LIGHT_FIX__ENABLED(true),
    SETTINGS__LIGHT_FIX__DELAY(20);

    private final Pl3xTables plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = Pl3xTables.getPlugin();
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("__", ".").replace("_", "-");
    }

    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }

    public int getInt() {
        return plugin.getConfig().getInt(getKey(), (int) def);
    }

    public static void reload() {
        Pl3xTables.getPlugin().reloadConfig();
    }
}
