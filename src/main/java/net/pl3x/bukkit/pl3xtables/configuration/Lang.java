package net.pl3x.bukkit.pl3xtables.configuration;

import java.io.File;
import net.pl3x.bukkit.pl3xtables.Logger;
import net.pl3x.bukkit.pl3xtables.Pl3xTables;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public enum Lang {
    NO_PERMISSION("&4You do not have permission for that command!"),
    TABLE_ALREADY_THERE("&4A table is already there!"),
    CANNOT_PLACE_BLOCKS_ON_TABLES("&4Cannot place blocks on top of tables!"),
    CANNOT_STACK_TABLES("&4Cannot stack tables on top of each other"),
    TABLE_ITEM_NAME("&bBlueprint&e: &aTable"),
    TABLE_ITEM_LORE("&d&oUse this blueprint to\n&d&ospawn a new &etable&d&o!"),
    VERSION("&d{plugin} v{version}."),
    RELOAD("&d{plugin} v{version} reloaded.");

    private final String def;

    private static File configFile;
    private static FileConfiguration config;

    Lang(String def) {
        this.def = def;
        reload();
    }

    public static void reload() {
        reload(false);
    }

    public static void reload(boolean force) {
        if (configFile == null || force) {
            String lang = Config.SETTINGS__LANGUAGE_FILE.getString();
            Logger.debug("Loading language file: " + lang);
            configFile = new File(Pl3xTables.getPlugin().getDataFolder(), lang);
            if (!configFile.exists()) {
                Pl3xTables.getPlugin().saveResource(Config.SETTINGS__LANGUAGE_FILE.getString(), false);
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    @Override
    public String toString() {
        String value = config.getString(name());
        if (value == null) {
            value = config.getString(getKey());
        }
        if (value == null) {
            Logger.warn("Missing lang data in file: " + getKey());
            value = def;
        }
        if (value == null) {
            Logger.error("Missing default lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}
