package net.pl3x.bukkit.pl3xtables.configuration;

import java.io.File;
import net.pl3x.bukkit.pl3xtables.Pl3xTables;
import net.pl3x.bukkit.pl3xtables.Position;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

public class PositionsConfig extends YamlConfiguration {
    private static PositionsConfig config;

    public static PositionsConfig getConfig() {
        if (config == null) {
            config = new PositionsConfig();
        }
        return config;
    }

    public static void reloadConfig() {
        config = null;
        getConfig();
    }

    private PositionsConfig() {
        super();
        try {
            load(new File(Pl3xTables.getPlugin().getDataFolder(), "positions.yml"));
        } catch (Exception ignore) {
        }
    }

    public Position getPosition(Material material, int corner) {
        String[] split = getString(material.name() + "." + corner, "").split(",");
        double offsetX, offsetY, offsetZ, yaw, rotX, rotY, rotZ;
        boolean small;
        int slot;
        try {
            offsetX = Double.parseDouble(split[0]);
            offsetY = Double.parseDouble(split[1]);
            offsetZ = Double.parseDouble(split[2]);
            yaw = Double.parseDouble(split[3]);
            rotX = Double.parseDouble(split[4]);
            rotY = Double.parseDouble(split[5]);
            rotZ = Double.parseDouble(split[6]);
            small = Integer.parseInt(split[7]) == 1;
            slot = Integer.parseInt(split[8]);
        } catch (NumberFormatException e) {
            return null;
        }
        return new Position(new Vector(offsetX, offsetY, offsetZ), yaw, new EulerAngle(rotX, rotY, rotZ), small, slot);
    }
}
