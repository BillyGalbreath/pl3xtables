package net.pl3x.bukkit.pl3xtables.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.pl3x.bukkit.pl3xtables.Logger;
import net.pl3x.bukkit.pl3xtables.Pl3xTables;
import net.pl3x.bukkit.pl3xtables.table.Table;
import net.pl3x.bukkit.pl3xtables.table.TableManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

public class TableConfig extends YamlConfiguration {
    private static final File directory = new File(Pl3xTables.getPlugin().getDataFolder(), "data");
    private static final Map<Location, TableConfig> configs = new HashMap<>();

    public static TableConfig getConfig(Location location) {
        TableConfig config = configs.get(location);
        if (config == null) {
            config = new TableConfig(location);
        }
        return config;
    }

    public static TableConfig getConfig(Table table) {
        return getConfig(table.getLocation());
    }

    public static void unloadConfigs() {
        Collection<TableConfig> oldConfs = new ArrayList<>(configs.values());
        synchronized (configs) {
            oldConfs.forEach(TableConfig::unload);
        }
    }

    public static void loadAll() {
        TableManager.getManager().unloadAll();

        File[] fileList = directory.listFiles((dir1, name) -> name.endsWith(".yml"));
        if (fileList == null) {
            return;
        }
        for (File file : fileList) {
            Location location = stringToLocation(file.getName().split("\\.yml")[0]);
            if (location == null) {
                Logger.warn("Unable to load file " + file.getName() + " (location conversion failed)");
                continue;
            }

            TableConfig tableConfig = TableConfig.getConfig(location);
            if (tableConfig == null) {
                Logger.warn("Unable to load file " + file.getName() + " (missing configuration)");
                continue;
            }

            Table table = tableConfig.getTable();
            if (table == null) {
                Logger.warn("Unable to load file " + file.getName() + " (invalid configuration)");
                continue;
            }

            Logger.debug("Loaded table at " + location);
            TableManager.getManager().addTable(table);
        }
    }

    private static String locationToString(Location location) {
        return location.getWorld().getName() + "_" + location.getBlockX() + "_" + location.getBlockY() + "_" + location.getBlockZ();
    }

    private static Location stringToLocation(String string) {
        String[] split = string.split("_");
        World world = Bukkit.getWorld(split[0]);
        if (world == null) {
            return null;
        }
        int x, y, z;
        try {
            x = Integer.parseInt(split[1]);
            y = Integer.parseInt(split[2]);
            z = Integer.parseInt(split[3]);
        } catch (NumberFormatException e) {
            return null;
        }
        return new Location(world, x, y, z);
    }

    private File file = null;
    private final Object saveLock = new Object();
    private final Location location;

    private TableConfig(Location location) {
        super();
        file = new File(directory, TableConfig.locationToString(location) + ".yml");
        this.location = location;
        reload();
    }

    private void reload() {
        synchronized (saveLock) {
            try {
                load(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void unload() {
        synchronized (configs) {
            configs.remove(location);
        }
    }

    public boolean delete() {
        unload();
        Logger.debug("Deleting table file: " + file.getAbsolutePath());
        if (file.exists() && file.delete()) {
            Logger.debug("Deleted file " + file.getAbsolutePath());
            return true;
        }
        Logger.warn("Unable to delete table data file: " + file.getAbsolutePath());
        return false;
    }

    public void setTable(Table table) {
        Location loc = table.getLocation();
        set("location.world", loc.getWorld().getName());
        set("location.x", loc.getBlockX());
        set("location.y", loc.getBlockY());
        set("location.z", loc.getBlockZ());

        setContents(table);
    }

    private Table getTable() {
        World world = Bukkit.getWorld(getString("location.world"));
        if (world == null) {
            return null;
        }

        int x, y, z;
        try {
            x = getInt("location.x");
            y = getInt("location.y");
            z = getInt("location.z");
        } catch (Exception e) {
            return null;
        }

        Table table = new Table(new Location(world, x, y, z));
        table.init();

        for (int i = 0; i < 4; i++) {
            try {
                table.addItem(null, getItemStack("contents." + i));
            } catch (Exception e) {
                Logger.warn("Invalid contents for table at " + table.getLocation());
                Logger.warn(e.getLocalizedMessage());
                e.printStackTrace();
            }
        }
        return table;
    }

    public void setContents(Table table) {
        List<ItemStack> contents = table.getItems();
        for (int i = 0; i < 4; i++) {
            set("contents." + i, contents.get(i));
            save();
        }
    }
}
