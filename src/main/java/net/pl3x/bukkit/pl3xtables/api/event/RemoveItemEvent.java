package net.pl3x.bukkit.pl3xtables.api.event;

import net.pl3x.bukkit.pl3xtables.table.Table;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("unused")
public class RemoveItemEvent extends TableEvent {
    private final ItemStack item;

    public RemoveItemEvent(Player player, Table table, ItemStack item) {
        super(player, table);
        this.item = item;
    }

    public ItemStack getItem() {
        return item;
    }
}
