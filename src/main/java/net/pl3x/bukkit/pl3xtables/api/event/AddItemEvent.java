package net.pl3x.bukkit.pl3xtables.api.event;

import net.pl3x.bukkit.pl3xtables.table.Table;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("WeakerAccess")
public class AddItemEvent extends TableEvent {
    private ItemStack item;

    public AddItemEvent(Player player, Table table, ItemStack item) {
        super(player, table);
        setItem(item);
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }
}
