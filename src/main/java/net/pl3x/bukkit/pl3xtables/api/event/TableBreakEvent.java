package net.pl3x.bukkit.pl3xtables.api.event;

import net.pl3x.bukkit.pl3xtables.table.Table;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class TableBreakEvent extends TableEvent {
    public TableBreakEvent(Player player, Table table) {
        super(player, table);
    }
}
