package net.pl3x.bukkit.pl3xtables.command;

import java.util.List;
import net.pl3x.bukkit.pl3xtables.Chat;
import net.pl3x.bukkit.pl3xtables.Logger;
import net.pl3x.bukkit.pl3xtables.Pl3xTables;
import net.pl3x.bukkit.pl3xtables.configuration.Config;
import net.pl3x.bukkit.pl3xtables.configuration.Lang;
import net.pl3x.bukkit.pl3xtables.configuration.PositionsConfig;
import net.pl3x.bukkit.pl3xtables.configuration.TableConfig;
import net.pl3x.bukkit.pl3xtables.table.TableManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

public class CmdPl3xTables implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.pl3xtables")) {
            new Chat(Lang.NO_PERMISSION).send(sender);
            return true;
        }

        if (args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("command.pl3xtables.reload")) {
                new Chat(Lang.NO_PERMISSION).send(sender);
                return true;
            }

            Logger.info("Reloading configs...");
            Config.reload();
            Lang.reload(true);
            PositionsConfig.reloadConfig();

            Logger.info("Unloading all tables...");
            TableManager.getManager().unloadAll();
            TableConfig.unloadConfigs();

            Logger.info("Loading all tables...");
            TableManager.getManager().loadAll();

            Logger.info("Done.");

            new Chat(Lang.RELOAD
                    .replace("{version}", Pl3xTables.getPlugin().getDescription().getVersion())
                    .replace("{plugin}", Pl3xTables.getPlugin().getName()))
                    .send(sender);
            return true;
        }

        new Chat(Lang.VERSION
                .replace("{plugin}", Pl3xTables.getPlugin().getName())
                .replace("{version}", Pl3xTables.getPlugin().getDescription().getVersion()))
                .send(sender);
        return true;
    }
}
