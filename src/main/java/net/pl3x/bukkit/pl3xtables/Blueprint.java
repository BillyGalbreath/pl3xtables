package net.pl3x.bukkit.pl3xtables;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import net.pl3x.bukkit.pl3xtables.configuration.Lang;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

public enum Blueprint {
    TABLE(Material.PAPER, Lang.TABLE_ITEM_NAME, Lang.TABLE_ITEM_LORE, "WWW", "S S", "S S",
            new HashMap<Character, Material>() {{
                put('W', Material.WOOD);
                put('S', Material.STICK);
            }});

    private final ItemStack item;

    Blueprint(Material material, Lang name, Lang lore, String line1, String line2, String line3, HashMap<Character, Material> ingredients) {
        item = new ItemStack(material);

        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name.toString());
        meta.setLore(Arrays.asList(lore.toString().split("\\n")));
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        item.setItemMeta(meta);
        item.addUnsafeEnchantment(Enchantment.ARROW_FIRE, 1);

        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape(line1, line2, line3);
        for (Map.Entry<Character, Material> ingredient : ingredients.entrySet()) {
            recipe.setIngredient(ingredient.getKey(), ingredient.getValue());
        }
        Bukkit.addRecipe(recipe);
    }

    public ItemStack getItem() {
        return item;
    }

    public boolean isItem(ItemStack stack) {
        if (stack == null) {
            return false; // null
        }

        if (!stack.hasItemMeta()) {
            return false; // does not have meta
        }

        ItemMeta stackMeta = stack.getItemMeta();
        if (!stackMeta.hasDisplayName()) {
            return false; // does not have a custom name
        }

        if (!stackMeta.hasLore()) {
            return false; // does not contain any lore
        }

        if (!stackMeta.getItemFlags().contains(ItemFlag.HIDE_ENCHANTS)) {
            return false; // does not have hide enchants attribute
        }

        if (!stackMeta.hasEnchant(Enchantment.ARROW_FIRE)) {
            return false; // does not have correct enchantment
        }

        ItemMeta itemMeta = item.getItemMeta();
        if (!stackMeta.getDisplayName().equals(itemMeta.getDisplayName())) {
            return false; // names do not match
        }

        //noinspection RedundantIfStatement
        if (!stackMeta.getLore().equals(itemMeta.getLore())) {
            return false; // lore does not match
        }

        return true; // items match
    }
}
