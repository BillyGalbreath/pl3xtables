package net.pl3x.bukkit.pl3xtables;

import net.pl3x.bukkit.pl3xtables.command.CmdPl3xTables;
import net.pl3x.bukkit.pl3xtables.configuration.Config;
import net.pl3x.bukkit.pl3xtables.configuration.Lang;
import net.pl3x.bukkit.pl3xtables.configuration.TableConfig;
import net.pl3x.bukkit.pl3xtables.listener.TableListener;
import net.pl3x.bukkit.pl3xtables.table.TableManager;
import net.pl3x.bukkit.pl3xtables.task.FixLight;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xTables extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();
        saveResource("positions.yml", false);
        Lang.reload();

        TableManager.getManager().loadAll();

        getServer().getPluginManager().registerEvents(new TableListener(), this);

        getCommand("pl3xtables").setExecutor(new CmdPl3xTables());

        int lightFixDelay = Config.SETTINGS__LIGHT_FIX__DELAY.getInt();
        FixLight.getTask().runTaskTimerAsynchronously(Pl3xTables.getPlugin(), lightFixDelay, lightFixDelay);

        Blueprint.TABLE.isItem(null); // populate blueprints and add recipes

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        FixLight.getTask().cancel();

        Logger.info("Unloading tables from world. Please wait...");
        TableManager.getManager().unloadAll();
        TableConfig.unloadConfigs();
        Logger.info("Unload complete.");

        Logger.info(getName() + " disabled.");
    }

    public static Pl3xTables getPlugin() {
        return Pl3xTables.getPlugin(Pl3xTables.class);
    }
}
